package com.pivinadanang.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class CategoryEntity extends BaseEntity {

  private String name;
  private String code;


  @OneToMany(mappedBy = "category")
  private List<PostEntity> posts = new ArrayList<>();


  public CategoryEntity() {
  }

  public CategoryEntity(String name, String code) {
    this.name = name;
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public List<PostEntity> getPosts() {
    return posts;
  }

  public void setPosts(List<PostEntity> posts) {
    this.posts = posts;
  }

  public void addPost(PostEntity postEntity) {
    postEntity.setCategory(this);
    this.posts.add(postEntity);
  }

  public void removePost(PostEntity postEntity) {
    this.posts.remove(postEntity);
    postEntity.setCategory(null);

  }
}
