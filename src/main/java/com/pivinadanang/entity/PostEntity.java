package com.pivinadanang.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "post")
public class PostEntity extends BaseEntity {

  @Column(name = "title")
  private String title;

  @Column(name = "shortdescription", columnDefinition = "TEXT")
  private String shortDescription;

  @Column(name = "content", columnDefinition = "TEXT")
  private String content;

  @Column(name = "thumbnailurl", columnDefinition = "TEXT")
  private String thumbnailUrl;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "category_id")
  private CategoryEntity category;

  @OneToMany(mappedBy = "post", orphanRemoval = true, cascade = CascadeType.REMOVE)
  private List<CommentEntity> comments = new ArrayList<>();

  public PostEntity() {
  }

  public PostEntity(String title, String shortDescription, String content, String thumbnailUrl,
      CategoryEntity category, List<CommentEntity> comments) {
    this.title = title;
    this.shortDescription = shortDescription;
    this.content = content;
    this.thumbnailUrl = thumbnailUrl;
    this.category = category;
    this.comments = comments;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  public CategoryEntity getCategory() {
    return category;
  }

  public void setCategory(CategoryEntity category) {
    this.category = category;
  }

  public List<CommentEntity> getComments() {
    return comments;
  }

  public void setComments(List<CommentEntity> comments) {
    this.comments = comments;
  }

  public void addComment(CommentEntity comment) {
    comments.add(comment);
    comment.setPost(this);
  }

  public void removeComment(CommentEntity comment) {
    comments.remove(comment);
    comment.setPost(null);
  }

  public void removeComment() {
    for (CommentEntity comment : new ArrayList<>(comments)) {
      removeComment(comment);
    }
  }
}
