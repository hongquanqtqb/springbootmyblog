package com.pivinadanang.exeption;

public class DuplicateRecordException extends RuntimeException {

  public DuplicateRecordException(String message) {
    super(message);
  }
}