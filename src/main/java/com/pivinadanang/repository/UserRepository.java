package com.pivinadanang.repository;

import com.pivinadanang.entity.UserEntity;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

  @Query(value = "select u from UserEntity u order by u.createdDate desc")
  Page<UserEntity> getUsersByUserName(Pageable pageable);


  @Query(value = "select u from UserEntity u where u.userName LIKE ?1% order by u.createdDate desc")
  Page<UserEntity> getUsersByUserNamePaginged(String username, Pageable pageable);

  @Query(value = "select u from UserEntity u where u.userName = ?1 and u.status = com.pivinadanang.enums.Status.ACTIVE")
  UserEntity findByUserNameAndStatus(String username);

  @Query(value = "select u from UserEntity u where u.userName = ?1")
  Optional<UserEntity> findByUserName(String username);

  @Modifying
  UserEntity save(UserEntity userEntity);

  @Query(value = "select u from UserEntity u where u.id = ?1")
  Optional<UserEntity> findById(Long id);

  @Modifying
  @Query(value = "delete from user u where u.id= :id", nativeQuery = true)
  void deleteUserById(@Param("id") Long id);
}
