package com.pivinadanang.repository;

import com.pivinadanang.entity.PostEntity;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<PostEntity, Long> {

  @Query("select p from PostEntity p order by p.createdDate desc")
  Page<PostEntity> getAllPosts(Pageable pageable);

  @Query("select p from PostEntity p where p.title LIKE ?1% order by p.createdDate desc")
  Page<PostEntity> findByTitle(String title, Pageable pageable);

  @Query("select p from PostEntity p where p.id = ?1")
  Optional<PostEntity> getPostById(Long id);

  @Modifying
  PostEntity save(PostEntity postEntity);

  @Modifying
  @Query(value = "delete from PostEntity p where p.id = ?1")
  void deleteById(Long id);

  @Query(value = "select count(p.id) from PostEntity p where p.category.id = ?1")
  int countByCategoryId(Long category_id);

  @Query(value = "select p from PostEntity p where p.category.name = ?1")
  Page<PostEntity> getPostsByCategoryName(String name, Pageable pageable);

  @Query(value = "select p from PostEntity p where p.category.id = ?1")
  Optional<PostEntity> getPostEntityByCategoryId(Long category_id);

  @Query(value = "select count(id) from PostEntity where category.id =:id")
  int countPostByCategoryId(@Param("id") Long id);

  @Modifying
  @Query(value = "delete from PostEntity p where p.category.id = ?1")
  void deleteByCategoryId(Long id);
}