package com.pivinadanang.repository;

import com.pivinadanang.entity.CommentEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<CommentEntity, Long> {

  @Query("select c from CommentEntity c order by c.createdDate desc")
  List<CommentEntity> findAllComment();

  @Modifying
  CommentEntity save(CommentEntity commentEntity);


  @Query("select c from CommentEntity c where c.id = ?1")
  Optional<CommentEntity> getCommentById(Long id);

  @Modifying
  @Query(value = "delete from CommentEntity c where c.id = ?1")
  void deleteCommentById(Long id);
}
