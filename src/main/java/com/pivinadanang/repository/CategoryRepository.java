package com.pivinadanang.repository;

import com.pivinadanang.entity.CategoryEntity;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

  @Query(value = "select c from CategoryEntity c order by c.createdDate desc ")
  Page<CategoryEntity> getAllCategoryPaginged(Pageable pageable);

  @Query(value = "select c from CategoryEntity c where c.name LIKE ?1% order by c.createdDate desc")
  Page<CategoryEntity> searchCategoryPaginged(String name, Pageable pageable);

  @Query(value = "select c from CategoryEntity c where c.code = ?1")
  Optional<CategoryEntity> findOneByCategoryCode(String code);

  @Query(value = "select c from CategoryEntity c where c.name = ?1")
  Optional<CategoryEntity> findOneByCategoryName(String name);

  @Query(value = "select c from CategoryEntity c where c.id = ?1")
  Optional<CategoryEntity> findByCategoryId(Long id);

  @Modifying
  @Query(value = "delete from CategoryEntity c where c.id = ?1")
  void deleteCategoryById(Long id);
}
