package com.pivinadanang.repository;

import com.pivinadanang.entity.RefreshTokenEntity;
import com.pivinadanang.entity.UserEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshTokenEntity, Long> {

  Optional<RefreshTokenEntity> findByToken(String token);

  @Modifying
  int deleteByUser(UserEntity userEntity);

}
