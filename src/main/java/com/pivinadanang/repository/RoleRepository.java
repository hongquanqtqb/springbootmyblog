package com.pivinadanang.repository;

import com.pivinadanang.entity.RoleEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

  @Query(value = "select r from RoleEntity r")
  List<RoleEntity> findAllRoles();

  @Query(value = "select r from RoleEntity r where r.name = ?1")
  Optional<RoleEntity> findByName(String name);

  @Query(value = "insert into user_role(userid,roleid) value(:userid,:roleid)", nativeQuery = true)
  void addRoleToUser(@Param("userid") Long userid, @Param("roleid") Long roleid);

  @Modifying
  @Query(value = "delete from role r where r.id= :id", nativeQuery = true)
  void deleteRoleById(@Param("id") Long id);
}
