package com.pivinadanang.constant;

public class SystemConstant {

  public static final String USER = "ROLE_USER";
  public static final String ADMIN = "ROLE_ADMIN";
  public static final String MANAGER = "ROLE_MANAGER";
  public static final String SADMIN = "ROLE_SUPER_ADMIN";
}
