package com.pivinadanang.security.service;

import com.pivinadanang.entity.UserEntity;
import com.pivinadanang.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserEntity user = userRepository.findByUserNameAndStatus(username);
    if (user == null) {
      throw new UsernameNotFoundException("User not found with username:" + username);
    }
    return CustomUserDetails.build(user);
  }
}
