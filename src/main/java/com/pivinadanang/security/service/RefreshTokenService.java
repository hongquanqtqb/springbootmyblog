package com.pivinadanang.security.service;

import com.pivinadanang.entity.RefreshTokenEntity;
import com.pivinadanang.exeption.TokenRefreshException;
import com.pivinadanang.repository.RefreshTokenRepository;
import com.pivinadanang.repository.UserRepository;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RefreshTokenService {

  @Value("${pivinadanang.app.jwtRefreshExpirationMs}")
  private Long refreshTokenDurationMs;

  @Autowired
  private RefreshTokenRepository refreshTokenRepository;

  @Autowired
  private UserRepository userRepository;

  public Optional<RefreshTokenEntity> findByToken(String token) {
    return refreshTokenRepository.findByToken(token);
  }

  public RefreshTokenEntity createRefreshToken(Long id) {
    RefreshTokenEntity refreshTokenEntity = new RefreshTokenEntity();

    refreshTokenEntity.setUser(userRepository.findById(id).get());
    refreshTokenEntity.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs));
    refreshTokenEntity.setToken(UUID.randomUUID().toString());

    refreshTokenEntity = refreshTokenRepository.save(refreshTokenEntity);
    return refreshTokenEntity;
  }

  public RefreshTokenEntity verifyExpiration(RefreshTokenEntity refreshToken) {
    if (refreshToken.getExpiryDate().compareTo(Instant.now()) < 0) {
      refreshTokenRepository.delete(refreshToken);
      throw new TokenRefreshException(refreshToken.getToken(),
          "Refresh token was expired. Please make a new signin request");
    }

    return refreshToken;
  }

  @Transactional
  public int deleteByUserId(Long id) {
    return refreshTokenRepository.deleteByUser(userRepository.findById(id).get());
  }
}
