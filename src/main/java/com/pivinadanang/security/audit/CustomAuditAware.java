package com.pivinadanang.security.audit;

import com.pivinadanang.security.service.CustomUserDetails;
import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class CustomAuditAware implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication == null || !authentication.isAuthenticated()) {
      return null;
    }
    if (authentication.getPrincipal().equals("anonymousUser")) {
      return Optional.of("anonymousUser");
    }
    return Optional.ofNullable(((CustomUserDetails) authentication.getPrincipal()).getUsername());
  }
}
