package com.pivinadanang.security.util;

import com.pivinadanang.security.service.CustomUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserUtil {

  public static CustomUserDetails getCurrentLoggedInUser() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    CustomUserDetails userDetails = (CustomUserDetails) auth.getPrincipal();
    return userDetails;
  }
}
