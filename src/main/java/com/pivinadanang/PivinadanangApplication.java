package com.pivinadanang;

import com.pivinadanang.security.audit.CustomAuditAware;
import java.util.Date;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class PivinadanangApplication {

  public static void main(String[] args) {
    SpringApplication.run(PivinadanangApplication.class, args);
  }

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();
//    modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
    modelMapper.createTypeMap(String.class, Date.class);
    modelMapper.getConfiguration().setSkipNullEnabled(true);
    return modelMapper;
  }

  @Bean
  public AuditorAware<String> auditorAware() {
    return new CustomAuditAware();
  }
}
