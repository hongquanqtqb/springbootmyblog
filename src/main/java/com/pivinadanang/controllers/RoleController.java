package com.pivinadanang.controllers;

import com.pivinadanang.dto.RoleDTO;
import com.pivinadanang.dto.RoleToUserForm;
import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.responses.ResponseObject;
import com.pivinadanang.service.IRoleService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1")
public class RoleController {

  @Autowired
  private IRoleService roleService;

  @GetMapping("roles")
  public ResponseEntity<ResponseObject> getAllRole() {
    List<RoleDTO> roles = roleService.findAllRoles();
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "Get all Role successful", ""));
  }

  @PostMapping("roles")
  public ResponseEntity<ResponseObject> createRole(@Valid @RequestBody RoleDTO role) {
    RoleDTO roleDTO = roleService.saveRole(role);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("201", "Create role successfuly", roleDTO));
  }

  @PostMapping("roles/addRoleToUser")
  public ResponseEntity<ResponseObject> addRoleToUser(
      @Valid @RequestBody RoleToUserForm roleToUserForm) {
    UserDTO user = roleService.addRoleToUser(roleToUserForm);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("201", "Add role to user successful", user));
  }

  @DeleteMapping("roles/{id}")
  public ResponseEntity<ResponseObject> deleteRoleById(@PathVariable Long id) {
    roleService.deleteRoleById(id);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "Delete role successfuly", ""));
  }
}
