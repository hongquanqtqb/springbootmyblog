package com.pivinadanang.controllers;

import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.responses.ResponseObject;
import com.pivinadanang.service.ICategoryService;
import com.pivinadanang.validation.OnCreate;
import com.pivinadanang.validation.OnUpdate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.groups.Default;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@Validated
public class CategoryController {

  @Autowired
  private ICategoryService categoryService;

  @GetMapping("/categories/search")
  @PreAuthorize("hasAnyRole('ROLE_USER') or hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<Map<String, Object>> getAllCategores(
      @RequestParam(value = "keyword", defaultValue = "", required = false) String keyword,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "10") int size) {
    try {
      Pageable pageable = PageRequest.of(page, size);
      Page<CategoryDTO> categoresPage;
      if (keyword.isEmpty()) {
        categoresPage = categoryService.getAllCategoryPaginged(pageable);
      } else {
        categoresPage = categoryService.searchCategoryPaginged(keyword, pageable);
      }
      List<CategoryDTO> categores = categoresPage.getContent();
      Map<String, Object> response = new HashMap<>();
      response.put("categores", categores);
      response.put("currentPage", categoresPage.getNumber());
      response.put("totalItems", categoresPage.getTotalElements());
      response.put("totalPages", categoresPage.getTotalPages());
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(value = "/categories/{id}")
  public ResponseEntity<?> getCategoryById(@PathVariable("id") @Min(1) Long id) {
    CategoryDTO category = categoryService.findByCategoryId(id);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "Search category by id successfully", category));
  }

  @PostMapping(value = "/categories")
  @PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  @Validated(OnCreate.class)
  public ResponseEntity<?> createCategory(@Valid @RequestBody CategoryDTO categoryDTO) {
    CategoryDTO ctegories = categoryService.createCategory(categoryDTO);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(new ResponseObject("201", "Create category successfully", ctegories));
  }

  @PutMapping(value = "/categories")
  public ResponseEntity<?> updateCategory(
      @Valid @RequestBody @Validated({OnUpdate.class, Default.class}) CategoryDTO categoryDTO) {
    CategoryDTO ctegories = categoryService.updateCategory(categoryDTO);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(new ResponseObject("201", "Update category successfully", ctegories));
  }

  @DeleteMapping(value = "/categories/{id}")
  public ResponseEntity<ResponseObject> deleteCategory(@PathVariable("id") @Min(1) Long id) {
    categoryService.deleteCategoryById(id);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "Delete posts successfully", ""));
  }
}
