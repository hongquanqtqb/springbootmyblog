package com.pivinadanang.controllers;

import com.pivinadanang.dto.TokenRefreshDTO;
import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.entity.RefreshTokenEntity;
import com.pivinadanang.exeption.TokenRefreshException;
import com.pivinadanang.responses.JwtResponse;
import com.pivinadanang.responses.ResponseObject;
import com.pivinadanang.responses.TokenRefreshResponse;
import com.pivinadanang.security.jwt.JwtUtils;
import com.pivinadanang.security.service.CustomUserDetails;
import com.pivinadanang.security.service.RefreshTokenService;
import com.pivinadanang.service.IUserService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "api/auth")
public class AuthenticationController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtUtils jwtUtils;

  @Autowired
  private IUserService userService;

  @Autowired
  RefreshTokenService refreshTokenService;

  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@RequestBody UserDTO userDTO) {
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(userDTO.getUserName(), userDTO.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);

    CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();

    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    RefreshTokenEntity refreshTokenEntity = refreshTokenService.createRefreshToken(
        userDetails.getId());

    return ResponseEntity.ok(new JwtResponse(jwt,
        refreshTokenEntity.getToken(),
        userDetails.getId(),
        userDetails.getUsername(),
        roles));
  }

  @PostMapping(value = "/signup")
  public ResponseEntity<?> registerUser(@Valid @RequestBody UserDTO request) {
    UserDTO user = userService.register(request);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("201", "Resgister user is successfully", user));
  }

  @PostMapping(value = "/refreshtoken")
  public ResponseEntity<?> refreshtoken(@RequestBody TokenRefreshDTO request) {
    String requestRefreshToken = request.getRefreshToken();
    return refreshTokenService.findByToken(requestRefreshToken)
        .map(refreshTokenService::verifyExpiration)
        .map(RefreshTokenEntity::getUser)
        .map(user -> {
          String acess_token = jwtUtils.generateTokenFromUsername(user.getUserName());
          return ResponseEntity.ok(new TokenRefreshResponse(acess_token, requestRefreshToken));
        })
        .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
            "Refresh token is not in database!"));

  }

  @PostMapping("/logout")
  public ResponseEntity<?> logoutUser() {
    CustomUserDetails userDetails = (CustomUserDetails) SecurityContextHolder.getContext()
        .getAuthentication().getPrincipal();
    Long userId = userDetails.getId();
    refreshTokenService.deleteByUserId(userId);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "Log out successful!", ""));
  }
}
