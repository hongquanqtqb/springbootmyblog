package com.pivinadanang.controllers;

import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.responses.ResponseObject;
import com.pivinadanang.service.IUserService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class UserController {

  @Autowired
  private IUserService userService;

  @GetMapping("users")
  public ResponseEntity<Map<String, Object>> getAllUsers(
      @RequestParam(value = "keyword", defaultValue = "") String keyword,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "10") int size) {
    List<UserDTO> users = new ArrayList<>();
    Pageable pageable = PageRequest.of(page, size);
    Page<UserDTO> usersPage;
    if (keyword.isEmpty()) {
      usersPage = userService.getUsersByUserName(pageable);
    } else {
      usersPage = userService.getUsersByUserNamePaginged(keyword, pageable);
    }
    users = usersPage.getContent();
    Map<String, Object> response = new HashMap<>();
    response.put("users", users);
    response.put("currentPage", usersPage.getNumber());
    response.put("totalItems", usersPage.getTotalElements());
    response.put("totalPages", usersPage.getTotalPages());
    return new ResponseEntity<>(response, HttpStatus.OK);
  }
  
  @DeleteMapping(value = "user/{id}")
  public ResponseEntity<ResponseObject> deleteById(@PathVariable Long id) {
    userService.deleteUserById(id);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "delete user successfully", ""));
  }
}
