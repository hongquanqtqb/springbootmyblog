package com.pivinadanang.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

  @GetMapping("upload")
  public String showUpLoadPage() {
    return "upload";
  }
}
