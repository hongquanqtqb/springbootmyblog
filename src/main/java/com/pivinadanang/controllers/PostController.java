package com.pivinadanang.controllers;

import com.pivinadanang.dto.PostDTO;
import com.pivinadanang.responses.ResponseObject;
import com.pivinadanang.service.impl.PostService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/v1")
public class PostController {

  @Autowired
  private PostService postService;

  @GetMapping("/posts/search")
  public ResponseEntity<Map<String, Object>> getAllPosts(
      @RequestParam(value = "keyword", defaultValue = "", required = false) String keyword,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "10") int size) {
    try {
      List<PostDTO> posts = new ArrayList<>();
      Pageable pageable = PageRequest.of(page, size);
      Page<PostDTO> postsPage;
      if (keyword.isEmpty()) {
        postsPage = postService.getAllPosts(pageable);
      } else {
        postsPage = postService.searchPost(keyword, pageable);
      }
      posts = postsPage.getContent();
      Map<String, Object> response = new HashMap<>();
      response.put("posts", posts);
      response.put("currentPage", postsPage.getNumber());
      response.put("totalItems", postsPage.getTotalElements());
      response.put("totalPages", postsPage.getTotalPages());
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/posts")
  public ResponseEntity<Map<String, Object>> getPostsByCategoryName(
      @RequestParam(value = "categoryName", defaultValue = "", required = false) String categoryName,
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "10") int size) {
    try {
      Pageable pageable = PageRequest.of(page, size);
      Page<PostDTO> postsPage;
      if (categoryName.isEmpty()) {
        postsPage = postService.getAllPosts(pageable);
      } else {
        postsPage = postService.getPostsByCategoryName(categoryName, pageable);
      }
      List<PostDTO> posts = postsPage.getContent();
      Map<String, Object> response = new HashMap<>();
      response.put("posts", posts);
      response.put("currentPage", postsPage.getNumber());
      response.put("totalItems", postsPage.getTotalElements());
      response.put("totalPages", postsPage.getTotalPages());
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(value = "/posts/{id}")
  @PreAuthorize("hasAnyRole('ROLE_USER') or hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_MANAGER') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<?> getPostById(@PathVariable("id") Long id) {
    PostDTO post = postService.getPostById(id);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "Search posts successfully", post));
  }

  @PostMapping(value = "/post")
  @PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<?> createPost(@Valid @RequestBody PostDTO postDTO) {
    PostDTO posts = postService.createPost(postDTO);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(new ResponseObject("201", "Create posts successfully", posts));
  }

  @PutMapping(value = "/post")
  @PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<?> updatePost(@Valid @RequestBody PostDTO postDTO) {
    PostDTO posts = postService.updatePost(postDTO);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(new ResponseObject("201", "Update posts successfully", posts));
  }

  @DeleteMapping(value = "/post")
  @PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<ResponseObject> deletePost(@RequestBody long[] ids) {
    postService.deleteByPostId(ids);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "Delete posts successfully", ""));
  }
}
