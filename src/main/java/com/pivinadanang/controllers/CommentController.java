package com.pivinadanang.controllers;

import com.pivinadanang.dto.CommentDTO;
import com.pivinadanang.responses.ResponseObject;
import com.pivinadanang.service.ICommentService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "api/v1")
public class CommentController {

  @Autowired
  private ICommentService commentService;

  @GetMapping(value = "comments")
  @PreAuthorize("hasAnyRole('ROLE_USER') or hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<ResponseObject> getAllComments() {
    List<CommentDTO> comments = commentService.getAllComment();
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("200", "Get comments successfully", comments));
  }

  @PostMapping(value = "comments")
  @PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<ResponseObject> addComment(@Valid @RequestBody CommentDTO commentDTO) {
    CommentDTO comment = commentService.createComment(commentDTO);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(new ResponseObject("201", "Create comment successfully", comment));
  }

  @PutMapping(value = "comments")
  @PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<ResponseObject> updateComment(@Valid @RequestBody CommentDTO commentDTO) {
    CommentDTO comment = commentService.updateComment(commentDTO);
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(new ResponseObject("201", "Update comment successfully", comment));
  }

  @DeleteMapping(value = "comments")
  @PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
  public ResponseEntity<ResponseObject> deleteComment(@RequestBody long[] ids) {
    commentService.deleteComment(ids);
    return ResponseEntity.status(HttpStatus.OK)
        .body(new ResponseObject("201", "Update comment successfully", ""));
  }
}
