package com.pivinadanang.responses;

import java.util.List;

public class JwtResponse {

  private String access_token;
  private String type = "Bearer";
  private String refresh_token;
  private Long id;
  private String username;
  private List<String> roles;

  public JwtResponse(String access_token, String refresh_token, Long id, String username,
      List<String> roles) {
    this.access_token = access_token;
    this.refresh_token = refresh_token;
    this.id = id;
    this.username = username;
    this.roles = roles;
  }

  public String getAccess_token() {
    return access_token;
  }

  public void setAccess_token(String access_token) {
    this.access_token = access_token;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getRefresh_token() {
    return refresh_token;
  }

  public void setRefresh_token(String refresh_token) {
    this.refresh_token = refresh_token;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(List<String> roles) {
    this.roles = roles;
  }
}
