package com.pivinadanang.dto;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostDTO extends BaseDTO<PostDTO> {

  @NotBlank(message = "title should not be blank")
  @Size(min = 3, message = "title should be at least 3 chars")
  private String title;

  @NotBlank(message = "shortDescription should not be blank")
  @Size(min = 3, message = "shortDescription should be at least 3 chars")
  private String shortDescription;

  @NotBlank(message = "content should not be blank")
  @Size(min = 3, message = "content should be at least 3 chars")
  private String content;

  private String thumbnailurl;

  private Long categoryID;

  private String categoryCode;

  private String categoryName;

  private List<CommentDTO> comments = new ArrayList<>();

}
