package com.pivinadanang.dto;

import com.pivinadanang.validation.ValidName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryDTO extends BaseDTO<CategoryDTO> {

  @NotBlank(message = "Name should not be blank")
  @Size(min = 3, message = "Name should be at least 3 chars")
  @ValidName
  private String name;

  @NotBlank(message = "Code should not be blank")
  @Size(min = 3, message = "Code should be at least 3 chars")
  private String code;

}
