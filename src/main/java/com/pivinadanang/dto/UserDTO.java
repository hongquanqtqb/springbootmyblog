package com.pivinadanang.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pivinadanang.enums.Status;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO extends BaseDTO<UserDTO> {

  @NotEmpty
  @Size(min = 3, message = "password should have at least 3 characters")
  private String userName;

  @NotEmpty
  @Size(min = 8, message = "password should have at least 8 characters")
  private String password;

  @NotEmpty
  @Size(min = 5, message = "user name should have at least 5 characters")
  private String fullName;

  private Status status;

  @JsonIgnore
  private List<String> role;

  private List<RoleDTO> roles = new ArrayList<>();
}
