package com.pivinadanang.service;

import com.pivinadanang.dto.PostDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IPostService {

  Page<PostDTO> getAllPosts(Pageable pageable);

  Page<PostDTO> searchPost(String keyword, Pageable pageable);

  Page<PostDTO> getPostsByCategoryName(String categoryName, Pageable pageable);

  PostDTO getPostsByCategoryId(Long category_id);

  PostDTO getPostById(Long id);

  PostDTO createPost(PostDTO post);

  PostDTO updatePost(PostDTO postDTO);

  void deleteByPostId(long[] ids);
}
