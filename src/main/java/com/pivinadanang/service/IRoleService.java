package com.pivinadanang.service;

import com.pivinadanang.dto.RoleDTO;
import com.pivinadanang.dto.RoleToUserForm;
import com.pivinadanang.dto.UserDTO;
import java.util.List;

public interface IRoleService {

  List<RoleDTO> findAllRoles();

  RoleDTO saveRole(RoleDTO roleDTO);

  UserDTO addRoleToUser(RoleToUserForm roleToUserForm);

  void deleteRoleById(Long id);

}
