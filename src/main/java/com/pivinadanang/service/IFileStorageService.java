package com.pivinadanang.service;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public interface IFileStorageService {

  void saveFile(MultipartFile file, SseEmitter emitter, String guid, String desc);
}
