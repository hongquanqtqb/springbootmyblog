package com.pivinadanang.service;

import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IUserService {

  UserDTO register(UserDTO request);

  UserDTO updateUser(UserDTO userDTO);

  UserDTO findByUserName(String username);

  UserEntity getUser(String username);

  void deleteUserById(Long id);

  Page<UserDTO> getUsersByUserName(Pageable pageable);

  Page<UserDTO> getUsersByUserNamePaginged(String userName, Pageable pageable);

}
