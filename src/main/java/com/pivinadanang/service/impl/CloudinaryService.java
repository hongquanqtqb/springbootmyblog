package com.pivinadanang.service.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.pivinadanang.dto.CloudinaryDTO;
import com.pivinadanang.service.ICloudinaryService;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CloudinaryService implements ICloudinaryService {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());


  @Autowired
  private Cloudinary cloudinary;

  @Override
  public CloudinaryDTO upload(MultipartFile file) {
    try {
      Map uploadResult = cloudinary.uploader().upload(file.getBytes(),
          ObjectUtils.asMap("folder", "SpringBoot/Blog", "resource_type", "auto"));
      CloudinaryDTO cloudinaryDTO = new CloudinaryDTO();
      cloudinaryDTO.setPublicId(uploadResult.get("public_id").toString());
      cloudinaryDTO.setUrl((String) uploadResult.get("secure_url"));
      return cloudinaryDTO;
    } catch (Exception ex) {
      logger.error(
          "failed to load to Cloudinary the image file: " + file.getName());
      logger.error(ex.getMessage());
      return null;
    }
  }

  @Override
  public void delete(String publicId) {
    try {
      Map destroy = this.cloudinary.uploader().destroy(publicId, ObjectUtils.emptyMap());
    } catch (Exception ex) {
      logger.error("failed to delete  Cloudinary the image file: ");
      logger.error(ex.getMessage());
    }
  }

  @Override
  public ByteArrayResource download(String publicId, int width, int height,
      boolean isAvatar) {
    String format = "jpg";
    Transformation transformation = new Transformation().width(width).height(height).crop("fill");
    if (isAvatar) {
      transformation = transformation.radius("max");
      format = "png";
    }
    String cloudUrl = cloudinary.url().secure(true).format(format)
        .transformation(transformation)
        .publicId(publicId)
        .generate();
    try {
      URL url = new URL(cloudUrl);
      InputStream inputStream = url.openStream();
      byte[] out = org.apache.commons.io.IOUtils.toByteArray(inputStream);
      ByteArrayResource resource = new ByteArrayResource(out);
      return resource;
    } catch (Exception ex) {
      ex.getMessage();
      return null;
    }
  }
}
