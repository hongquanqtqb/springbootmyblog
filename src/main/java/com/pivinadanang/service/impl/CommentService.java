package com.pivinadanang.service.impl;

import com.pivinadanang.dto.CommentDTO;
import com.pivinadanang.entity.CommentEntity;
import com.pivinadanang.entity.PostEntity;
import com.pivinadanang.exeption.NotFoundExeption;
import com.pivinadanang.repository.CommentRepository;
import com.pivinadanang.repository.PostRepository;
import com.pivinadanang.service.ICommentService;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CommentService implements ICommentService {

  @Autowired
  private CommentRepository commentRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private PostRepository postRepository;

  @Override
  public List<CommentDTO> getAllComment() {
    List<CommentDTO> comments = commentRepository.findAllComment()
        .stream().map(CommentEntity -> modelMapper.map(CommentEntity, CommentDTO.class))
        .collect(Collectors.toList());
    ;
    return comments;
  }

  @Override
  @Transactional
  public CommentDTO createComment(CommentDTO commentDTO) {
    PostEntity post = postRepository.getPostById(commentDTO.getPostId())
        .orElseThrow(() -> new NotFoundExeption("Post not found with id" + commentDTO.getPostId()));
    CommentEntity commentEntity = modelMapper.map(commentDTO, CommentEntity.class);
    return modelMapper.map(commentRepository.save(commentEntity), CommentDTO.class);
  }

  @Override
  @Transactional
  public CommentDTO updateComment(CommentDTO commentDTO) {
    CommentEntity commentEntityOld = commentRepository.getCommentById(commentDTO.getId())
        .orElseThrow(() -> new NotFoundExeption("Comment not found with id" + commentDTO.getId()));
    ;
    this.modelMapper.map(commentDTO, commentEntityOld);
    return modelMapper.map(this.commentRepository.save(commentEntityOld), CommentDTO.class);
  }

  @Override
  @Transactional
  public void deleteComment(long[] ids) {
    for (Long id : ids) {
      commentRepository.deleteCommentById(id);
    }
  }
}
