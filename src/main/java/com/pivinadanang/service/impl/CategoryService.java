package com.pivinadanang.service.impl;

import com.pivinadanang.dto.CategoryDTO;
import com.pivinadanang.entity.CategoryEntity;
import com.pivinadanang.exeption.DuplicateRecordException;
import com.pivinadanang.exeption.NotFoundExeption;
import com.pivinadanang.repository.CategoryRepository;
import com.pivinadanang.repository.PostRepository;
import com.pivinadanang.service.ICategoryService;
import java.util.Optional;
import java.util.function.Function;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryService implements ICategoryService {

  private final ModelMapper modelMapper;

  private final CategoryRepository categoryRepository;

  @Autowired
  private PostRepository postRepository;

  @Autowired
  public CategoryService(ModelMapper modelMapper,
      CategoryRepository categoryRepository) {
    this.modelMapper = modelMapper;
    this.categoryRepository = categoryRepository;
  }

  @Override
  public Page<CategoryDTO> getAllCategoryPaginged(Pageable pageable) {
    Page<CategoryEntity> categoryEntityPage = categoryRepository.getAllCategoryPaginged(pageable);
    return categoryEntityPage.map(new Function<CategoryEntity, CategoryDTO>() {
      @Override
      public CategoryDTO apply(CategoryEntity t) {
        return new ModelMapper().map(t, CategoryDTO.class);
      }
    });
  }

  @Override
  public Page<CategoryDTO> searchCategoryPaginged(String name, Pageable pageable) {
    Page<CategoryEntity> categoryEntityPage = categoryRepository.searchCategoryPaginged(name,
        pageable);
    return categoryEntityPage.map(new Function<CategoryEntity, CategoryDTO>() {
      @Override
      public CategoryDTO apply(CategoryEntity t) {
        return new ModelMapper().map(t, CategoryDTO.class);
      }
    });
  }

  @Override
  public CategoryDTO findByCategoryId(Long id) {
    CategoryEntity categoryEntity = categoryRepository.findByCategoryId(id)
        .orElseThrow(() -> new NotFoundExeption("Category not found with id :" + id));
    return modelMapper.map(categoryEntity, CategoryDTO.class);
  }

  @Override
  public CategoryDTO findByCategoryName(String name) {
    CategoryEntity category = categoryRepository.findOneByCategoryName(name)
        .orElseThrow(() -> new NotFoundExeption("Category not found with name" + name));
    return modelMapper.map(category, CategoryDTO.class);
  }

  @Override
  @Transactional
  public CategoryDTO createCategory(CategoryDTO categoryDTO) {
    Optional<CategoryEntity> category = categoryRepository.findOneByCategoryCode(
        categoryDTO.getCode());
    if (category.isPresent()) {
      throw new DuplicateRecordException("Duplicate Record with code" + categoryDTO.getCode());
    } else {
      CategoryEntity categoryEntity = modelMapper.map(categoryDTO, CategoryEntity.class);
      return modelMapper.map(categoryRepository.save(categoryEntity), CategoryDTO.class);
    }
  }

  @Override
  @Transactional
  public CategoryDTO updateCategory(CategoryDTO categoryDTO) {
    CategoryEntity categoryEntityOld = this.categoryRepository.findByCategoryId(categoryDTO.getId())
        .orElseThrow(
            () -> new NotFoundExeption("Category not found with id :" + categoryDTO.getId()));
    ;
    this.modelMapper.map(categoryDTO, categoryEntityOld);
    return modelMapper.map(this.categoryRepository.save(categoryEntityOld), CategoryDTO.class);
  }

  @Override
  @Transactional
  public void deleteCategoryById(Long id) {
    CategoryEntity categoryEntity = categoryRepository.findByCategoryId(id)
        .orElseThrow(() -> new NotFoundExeption("Category not found with id :" + id));
    int countPostByCategoryId = postRepository.countPostByCategoryId(id);
    if (countPostByCategoryId > 0) {
      postRepository.deleteByCategoryId(categoryEntity.getId());
    }
    categoryRepository.deleteCategoryById(id);
  }
}
