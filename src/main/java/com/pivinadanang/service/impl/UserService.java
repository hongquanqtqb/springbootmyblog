package com.pivinadanang.service.impl;

import com.pivinadanang.constant.SystemConstant;
import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.entity.RoleEntity;
import com.pivinadanang.entity.UserEntity;
import com.pivinadanang.enums.Status;
import com.pivinadanang.exeption.DuplicateRecordException;
import com.pivinadanang.exeption.NotFoundExeption;
import com.pivinadanang.repository.RoleRepository;
import com.pivinadanang.repository.UserRepository;
import com.pivinadanang.service.IUserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class UserService implements IUserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  @Autowired
  private RoleRepository roleRepository;

  @Override
  public UserDTO register(UserDTO signupRequest) {
    Optional<UserEntity> userExist = userRepository.findByUserName(signupRequest.getUserName());
    if (userExist.isPresent()) {
      throw new DuplicateRecordException(
          "Username is already taken!: " + signupRequest.getUserName());
    }
    signupRequest.setPassword(passwordEncoder.encode(signupRequest.getPassword()));

    UserEntity userEntity = modelMapper.map(signupRequest, UserEntity.class);
    userEntity.setStatus(Status.ACTIVE);

    List<String> strRoles = signupRequest.getRole();
    List<RoleEntity> roles = new ArrayList<>();

    if (strRoles == null) {
      RoleEntity userRole = roleRepository.findByName(SystemConstant.USER)
          .orElseThrow(() -> new NotFoundExeption("Role is not found"));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
          case "ADMIN":
            RoleEntity adminRole = roleRepository.findByName(SystemConstant.ADMIN)
                .orElseThrow(() -> new NotFoundExeption("Role is not found"));
            roles.add(adminRole);
            break;
          case "MANAGER":
            RoleEntity managerRole = roleRepository.findByName(SystemConstant.MANAGER)
                .orElseThrow(() -> new NotFoundExeption("Role is not found"));
            roles.add(managerRole);
            break;
          case "SUPER-ADMIN":
            RoleEntity superAdminRole = roleRepository.findByName(SystemConstant.SADMIN)
                .orElseThrow(() -> new NotFoundExeption("Role is not found"));
            roles.add(superAdminRole);
            break;
          default:
            RoleEntity userRole = roleRepository.findByName(SystemConstant.USER)
                .orElseThrow(() -> new NotFoundExeption("Role is not found"));
            roles.add(userRole);
            break;
        }
      });
    }
    userEntity.setRoles(roles);
    UserEntity userEntityRequest = userRepository.save(userEntity);
    return modelMapper.map(userEntityRequest, UserDTO.class);

  }

  @Override
  public UserDTO updateUser(UserDTO userDTO) {
    return null;
  }

  @Override
  public UserDTO findByUserName(String username) {
    Optional<UserEntity> entity = userRepository.findByUserName(username);
    return modelMapper.map(entity, UserDTO.class);
  }

  @Override
  public UserEntity getUser(String username) {
    UserEntity entity = userRepository.findByUserName(username)
        .orElseThrow(() -> new NotFoundExeption("user not found with username: " + username));
    return entity;
  }

  @Override
  public void deleteUserById(Long id) {
    UserEntity userEntity = userRepository.findById(id)
        .orElseThrow(() -> new NotFoundExeption("user not found with id: " + id));
    userEntity.removeRole();
    userRepository.deleteUserById(id);
  }

  @Override
  public Page<UserDTO> getUsersByUserName(Pageable pageable) {
    Page<UserEntity> userEntityPage = userRepository.getUsersByUserName(pageable);

    return userEntityPage.map(new Function<UserEntity, UserDTO>() {
      @Override
      public UserDTO apply(UserEntity t) {
        return new ModelMapper().map(t, UserDTO.class);
      }
    });
  }

  @Override
  public Page<UserDTO> getUsersByUserNamePaginged(String username, Pageable pageable) {
    Page<UserEntity> userEntityPage = userRepository.getUsersByUserNamePaginged(username, pageable);

    return userEntityPage.map(new Function<UserEntity, UserDTO>() {
      @Override
      public UserDTO apply(UserEntity t) {
        return new ModelMapper().map(t, UserDTO.class);
      }
    });
  }


}
