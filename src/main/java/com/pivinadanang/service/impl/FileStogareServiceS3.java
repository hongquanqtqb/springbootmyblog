package com.pivinadanang.service.impl;

import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.pivinadanang.service.IFileStorageService;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Service
@Slf4j
public class FileStogareServiceS3 implements IFileStorageService {

  @Value("${amazon.region}")
  private String region;

  @Value("${amazon.s3.bucket}")
  private String bucket;


  @Value("${amazon.s3.endpoinUrl}")
  private String endpoinUrl;

  @Autowired
  TransferManager transferManager;

  @Override
  public void saveFile(MultipartFile file, SseEmitter emitter, String guid, String desc) {
    if (file.isEmpty()) {
      throw new IllegalStateException("Cannot upload empty file");
    }
    try {
      ObjectMetadata metadata = new ObjectMetadata();
      long totalbytes = file.getSize();
      metadata.setContentType(file.getContentType());
      metadata.setContentLength(totalbytes);
      PutObjectRequest request = new PutObjectRequest(bucket, file.getOriginalFilename(),
          file.getInputStream(), metadata);
      request.setGeneralProgressListener(new ProgressListener() {
        long accumulatedByteTransfered = 0;

        @Override
        public void progressChanged(ProgressEvent progressEvent) {
          accumulatedByteTransfered += progressEvent.getBytesTransferred();
          long progress = 100 * accumulatedByteTransfered / totalbytes;
          try {
            emitter.send(SseEmitter.event().name(guid).data(progress));
          } catch (IOException e) {
            log.info(e.getMessage());
          }
        }
      });
      transferManager.upload(request);
      String fileName = String.format("%s", file.getOriginalFilename());
      String url = endpoinUrl + "/" + bucket + "/" + fileName;
      System.out.println(url);
    } catch (IOException e) {
    }
  }
}
