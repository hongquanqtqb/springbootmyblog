package com.pivinadanang.service.impl;

import com.pivinadanang.dto.PostDTO;
import com.pivinadanang.entity.CategoryEntity;
import com.pivinadanang.entity.CommentEntity;
import com.pivinadanang.entity.PostEntity;
import com.pivinadanang.exeption.NotFoundExeption;
import com.pivinadanang.repository.CategoryRepository;
import com.pivinadanang.repository.CommentRepository;
import com.pivinadanang.repository.PostRepository;
import com.pivinadanang.service.IPostService;
import java.util.Optional;
import java.util.function.Function;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class PostService implements IPostService {

  @Autowired
  private PostRepository postRepository;
  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private CategoryRepository categoryRepository;

  @Autowired
  private CommentRepository commentRepository;


  @Override
  public Page<PostDTO> getAllPosts(Pageable pageable) {
    Page<PostEntity> postEntityPage = postRepository.getAllPosts(pageable);
    return postEntityPage.map(new Function<PostEntity, PostDTO>() {
      @Override
      public PostDTO apply(PostEntity t) {
        return new ModelMapper().map(t, PostDTO.class);
      }
    });
  }

  @Override
  public Page<PostDTO> searchPost(String keyword, Pageable pageable) {
    Page<PostEntity> postEntityPage = postRepository.findByTitle(keyword, pageable);
    return postEntityPage.map(new Function<PostEntity, PostDTO>() {
      @Override
      public PostDTO apply(PostEntity t) {
        return new ModelMapper().map(t, PostDTO.class);
      }
    });
  }

  @Override
  public Page<PostDTO> getPostsByCategoryName(String categoryName, Pageable pageable) {
    Page<PostEntity> postEntityPage = postRepository.getPostsByCategoryName(categoryName, pageable);
    return postEntityPage.map(new Function<PostEntity, PostDTO>() {
      @Override
      public PostDTO apply(PostEntity t) {
        return new ModelMapper().map(t, PostDTO.class);
      }
    });
  }

  @Override
  public PostDTO getPostsByCategoryId(Long category_id) {
    Optional<PostEntity> postEntity = postRepository.getPostEntityByCategoryId(category_id);
    return modelMapper.map(postEntity, PostDTO.class);
  }

  @Override
  public PostDTO getPostById(Long id) {
    PostEntity postEntity = postRepository.getPostById(id)
        .orElseThrow(() -> new NotFoundExeption("Post not found with id :" + id));
    return modelMapper.map(postEntity, PostDTO.class);
  }

  @Override
  @Transactional
  public PostDTO createPost(PostDTO post) {
    CategoryEntity category = categoryRepository.findOneByCategoryName(post.getCategoryName())
        .orElseThrow(() -> new NotFoundExeption(
            "Category not found with categoryCode :" + post.getCategoryName()));
    PostEntity postEntity = modelMapper.map(post, PostEntity.class);
    postEntity.setCategory(category);
    return modelMapper.map(postRepository.save(postEntity), PostDTO.class);
  }

  @Override
  @Transactional
  public PostDTO updatePost(PostDTO postDTO) {
    CategoryEntity category = new CategoryEntity();
    if (postDTO.getCategoryName() != null) {
      category = categoryRepository.findOneByCategoryName(postDTO.getCategoryName())
          .orElseThrow(() -> new NotFoundExeption(
              "Category not found with name" + postDTO.getCategoryName()));
    }
    PostEntity postEntityOld = postRepository.getPostById(postDTO.getId())
        .orElseThrow(() -> new NotFoundExeption(
            "Post not found with id :" + postDTO.getId()));
    ;
    postEntityOld.setCategory(category);
    this.modelMapper.map(postDTO, postEntityOld);
    return modelMapper.map(this.postRepository.save(postEntityOld), PostDTO.class);
  }

  @Override
  @Transactional
  public void deleteByPostId(long[] ids) {
    if (ids.length <= 0) {
      throw new RuntimeException("id null");
    } else {
      for (Long id : ids) {
        PostEntity postEntity = postRepository.getPostById(id)
            .orElseThrow(() -> new NotFoundExeption("Post not fount with id: " + id));
        for (CommentEntity comment : postEntity.getComments()) {
          commentRepository.deleteCommentById(comment.getId());
        }
        postRepository.deleteById(id);
      }
    }
  }
}

