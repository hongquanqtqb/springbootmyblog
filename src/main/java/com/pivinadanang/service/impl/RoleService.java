package com.pivinadanang.service.impl;

import com.pivinadanang.dto.RoleDTO;
import com.pivinadanang.dto.RoleToUserForm;
import com.pivinadanang.dto.UserDTO;
import com.pivinadanang.entity.RoleEntity;
import com.pivinadanang.entity.UserEntity;
import com.pivinadanang.exeption.NotFoundExeption;
import com.pivinadanang.repository.RoleRepository;
import com.pivinadanang.repository.UserRepository;
import com.pivinadanang.service.IRoleService;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService implements IRoleService {

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private UserRepository userRepository;

  @Override
  public List<RoleDTO> findAllRoles() {
    List<RoleDTO> roles = roleRepository.findAllRoles()
        .stream()
        .map(RoleEntity -> modelMapper.map(RoleEntity, RoleDTO.class))
        .collect(Collectors.toList());
    return roles;
  }

  @Override
  @Transactional
  public RoleDTO saveRole(RoleDTO roleDTO) {
    RoleEntity roleEntity = modelMapper.map(roleDTO, RoleEntity.class);
    return modelMapper.map(roleRepository.save(roleEntity), RoleDTO.class);
  }

  @Override
  @Transactional
  public UserDTO addRoleToUser(RoleToUserForm roleToUserForm) {
    UserEntity user = userRepository.findByUserName(roleToUserForm.getUsername())
        .orElseThrow(() -> new NotFoundExeption(
            "Cannot find user with username: " + roleToUserForm.getUsername()));

    RoleEntity role = roleRepository.findByName(roleToUserForm.getRolename())
        .orElseThrow(() -> new NotFoundExeption(
            "Cannot find role with rolename: " + roleToUserForm.getRolename()));
    user.addRole(role);
    return modelMapper.map(userRepository.save(user), UserDTO.class);
  }

  @Override
  public void deleteRoleById(Long id) {
    roleRepository.deleteRoleById(id);
  }
}
