package com.pivinadanang.service;

import com.pivinadanang.dto.CommentDTO;
import java.util.List;

public interface ICommentService {

  List<CommentDTO> getAllComment();

  CommentDTO createComment(CommentDTO commentDTO);

  CommentDTO updateComment(CommentDTO commentDTO);

  void deleteComment(long[] id);
}
