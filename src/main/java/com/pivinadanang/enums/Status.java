package com.pivinadanang.enums;

public enum Status {
  PENDING("P"), ACTIVE("A"), DISABLE("D"), REMOVE("R");

  private String code;

  private Status(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }
}